import java.util.HashMap;

public class Main {

    private HashMap<String, User> users;    //Key->String: Name of User; Value->User: User-Object
    private MainGui gui;

    public Main() {
        this.users = new HashMap<String, User>();
        this.gui = new MainGui();   //Builds Main Window
    }

    //created new user and adds it to field users. Called by GUI
    public void addUser(String userName) {

    }

    //removes existing user from fiels users. Called by User
    public void removeUser(User user) {

    }

    public HashMap<String, User> getUsers() {
        return users;
    }


    //calls update() of every existing user when new user is added or existing user is removed.
    private void notifyUsers(boolean added) {

    }

    //closes all windows.
    private void closeAll() {

    }

    //saves current state. Writes user-objects into a File to reload the objects
    private void saveAll() {

    }

    //when previous chat was saved: reloads (creates) user-objects from file and adds them to field users
    private void load() {

    }

    //main-function
    public static void main(String[] args) {

    }

}
