import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class User implements Serializable {

    private String name;
    private ArrayList<String> messages;
    private HashMap<String, ArrayList<String>> usergroups;  //Key->String: Name of usergroup; Value->ArrayList<String>: Names of users in group
    private Main main;
    private UserGui gui;

    public User(String userName, Main main) {
        this.name = userName;
        this.messages = new ArrayList<String>();
        this.usergroups = new HashMap<String, ArrayList<String>>();
        this.main = main;
        this.gui = new UserGui();
    }

    //sends message to all selected users. Will get users from Main selected users from GUI (JCheckBox isSelected).
    public void send(String msg) {

    }

    //will recieve a message sent from another user. Writes it in field messages and updates the GUI by calling the GUI.
    public void recieve(String msg) {

    }

    //updates field userNames. Will be called by notifyUsers() in Main
    public void update() {

    }

    //defines a new usergroup and updates the GUI by calling the GUI.
    public void defineUsergroup(ArrayList<String> userNames) {

    }

    //closes the Chat-Window properly. Will be called by GUI, when it's closed. Calls removeUser() in Main.
    public void close() {

    }


}
